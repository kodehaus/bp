/* js/menu-navigation.js */
$(".sicpa-button-burger").click(function () {
    if ($('.sicpa-navbar-left-content.help').hasClass('opened')) {
        $('.sicpa-navbar-left-content').removeClass('help-open');
        $('.sicpa-navbar-left-content.help').removeClass('opened');
    } else {
        $('.sicpa-navbar-left').toggleClass('opened');
        $('.sicpa-content').toggleClass('sicpa-content-side-menu-opened');
    }
});

$(".sicpa-button-help").click(function () {
    if (!$('.sicpa-navbar-left-content.help').hasClass('opened') && $('.sicpa-navbar-left').hasClass('opened')) {
        $('.sicpa-navbar-left-content').toggleClass('help-open');
        $('.sicpa-navbar-left-content.help').toggleClass('opened');
    }
    else {
        $('.sicpa-navbar-left-content').toggleClass('help-open');
        $('.sicpa-navbar-left-content.help').toggleClass('opened');
        $('.sicpa-content').toggleClass('sicpa-content-side-menu-opened');
        $('.sicpa-navbar-left').toggleClass('opened');
    }
});