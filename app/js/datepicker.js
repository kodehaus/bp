
new Pikaday(
    {
        field: document.getElementById('datepicker'),
        trigger: document.getElementById('datepicker-trigger'),
         theme: 'sicpa-theme',
         position: 'bottom right',
         minDate: new Date(2000, 0, 1),
        maxDate: new Date(2020, 12, 31),
        yearRange: [2010,2020]
    });

