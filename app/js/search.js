var selectOnUnFocus = function(selector) {
    return function() {
        $(selector).closest('div').find('label').removeClass('focus');
    };
};

var selectOnFocus = function(selector) {
    return function() {
        $(selector).closest('div').find('label').addClass('focus');
    };
};

var selectOnChange = function(selector) {
    return function() {
        if($(selector+'.selectize-control').find('.item').length > 0){
            $('select'+selector).parent().find('label').addClass('focus full');
            var length = $(selector+'.selectize-control').find('.item').length;
                $(selector+'.selectize-control').find('.item').each(function( index,obj ) {
                    if(index != length -1 && length > 1){
                        console.log(index);
                        $(obj).text($(obj).text()+',');
                        $(obj).text( $(obj).text().replace(',,',','));
                    }
                    if(index === 0 && length == 1){
                        $(obj).text( $(obj).text().replace(',',''));
                    }
                });
        }
    };
};


$('.select1').selectize({
    create: true,
    persist: true,
    sortField: {
        field: 'text',
        direction: 'asc'
    },
    dropdownParent: 'body',
    onFocus         : selectOnFocus('.select1'),
    onBlur          : selectOnUnFocus('.select1'),
    onChange        : selectOnChange('.select1'),
});


$('.select2').selectize({
    create: true,
    persist: true,
    sortField: {
        field: 'text',
        direction: 'asc'
    },
    dropdownParent: 'body',
    onFocus         : selectOnFocus('.select2'),
    onBlur          : selectOnUnFocus('.select2'),
    onChange        : selectOnChange('.select2'),
});

