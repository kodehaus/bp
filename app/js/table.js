/* js/table.js */
$('.sicpa-table tbody :checkbox').click(function () {

    $(this).parent().parent().toggleClass('selected');
});

$('.sicpa-table thead :checkbox').click(function () {

    $(this).parent().parent().parent().parent().find('tbody :checkbox').click();
    $(this).parent().parent().parent().parent().find('tbody :checkbox');
});

$('.sicpa-table span').bind('mouseenter', function(){
    var $this = $(this);


    if(this.offsetWidth < this.scrollWidth && !$this.attr('title')){
        $this.addClass('sicpa-tooltip');
        $this.attr('data-original-title', $this.text());
        $this.attr('data-toggle', 'tooltip');
        $this.attr('data-placement','top');
        $this.tooltip('show');

    }
});
