var selectOnUnFocus = function (selector) {
    return function () {
        selector.closest('div').find('label').removeClass('focus');
    };
};

var selectOnFocus = function (selector) {
    return function () {
        selector.closest('div').find('label').addClass('focus');
    };
};

var selectOnChange = function (selector) {
    return function () {
        if (selector.next('.selectize-control').find('.item').length > 0) {
            selector.closest('div').find('label').addClass('focus full');
            var length = selector.next('.selectize-control').find('.item').length;
            selector.next('.selectize-control').find('.item').each(function (index, obj) {
                if (index != length - 1 && length > 1) {
                    $(obj).text($(obj).text() + ',');
                    $(obj).text($(obj).text().replace(',,', ','));
                }
                if (index === 0 && length === 1) {
                    $(obj).text($(obj).text().replace(',', ''));
                }
            });
        } else {
            selector.closest('div').find('label').removeClass('focus full');
        }
    };
};

$('.sicpa-select').each(function () {
    $(this).selectize(
        {
            create        : false,
            persist       : true,
            sortField     : {
                field     : 'text',
                direction : 'asc'
            },
            dropdownParent: 'body',
            onFocus       : selectOnFocus($(this)),
            onBlur        : selectOnUnFocus($(this)),
            onChange      : selectOnChange($(this))
        });
});